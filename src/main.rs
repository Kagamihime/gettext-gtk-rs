extern crate gtk;
extern crate gettextrs;

use gtk::prelude::*;

use gettextrs::*;

fn main() {
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("test", "./po");
    textdomain("test");

    gtk::init().unwrap();

    let window: gtk::Window = gtk::Builder::new_from_file("./ui/main_window.glade")
        .get_object("main_window").expect("Failed to load the main window");

    // UI initialization
    let label = gtk::Label::new(gettext("A label from the source code").as_str());
    window.add(&label);
    window.show_all();

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    gtk::main();
}
